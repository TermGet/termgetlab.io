# Downloads

To get a stable build, [click here](http://termget.gitlab.io/Downloads/)

To get a alpha build, go to the [TermGet GitLab repository](https://gitlab.com/TermGet/TermGet), and click the download button on the top right corner.
