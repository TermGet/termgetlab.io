# Changelog

# TermGet 2.2

- Fixed Syntax Error
- Translated to German! (Thanks to Emil Engler)

# TermGet 2.1.4 (TermGet 2.1.3 with updated readme)

- Updated readme

# TermGet 2.1.3

- Code cleanup
- PIP changes
- Fixed installer
- Updated readme

# TermGet 2.1.2

- Code cleanup

# TermGet 2.1.0 - 2.1.1

- Added package manager detection to the installer.
- Fixed search
- Fix bugs with color
- Added a cool start screen
- Tons and tons of bug fixes
- Code cleanup

# TermGet 2.0.0

- Code cleaned up
- Bug fixes
- Added PIP support (pip, pip2, and pip3)
- Added APM support
- Added emerge
- Added pkg
- Added chromebrew
- Added chromebrew
- New interface for packages managers that aren't preinstalled (Such as PIP, and APM).
- Asks if you would like to use "apt-get remove" or "apt-get purge".
- Added color support
- Messed up search on apt lol :P
- Removed Herobrine

# TermGet 1.1.1 - 1.1.4

- Bug fixes

# TermGet 1.1.0

- Now it saves your packages
- Bug fixes

# TermGet 1.0.0

- First version of TermGet :)
- Added apt
- Added xbps
- Added pacman
- Added dnf
- Added zypper
- Added eopkg
- Added search feature
- Added install feature
- Added remove feature
- Added update packages feature
- Added update repo feature
- Added clean Feature
- Removed Herobrine