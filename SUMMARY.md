# Summary

* [Introduction](README.md)
* [Downloads](download.md)
* ------------------------------------------
* [Install and First Time Setup (Linux, BSD, and MacOS)](install.md)
* [Install and First Time Setup (Chrome OS)](install-chrome.md)
* [How to use TermGet](use.md)
* [Change Package Manager](package-manager.md)
* [TermGet for Windows](TermGet-Windows.md)
* [License](License.md)
* ------------------------------------------
* [Changelog](Changelog.md)
* [To Do List for TermGet 3.0](ToDo.md)
* ------------------------------------------
* [Discord](https://discord.gg/QJvC2AA)
* [Gitlab](https://gitlab.com/TermGet/)