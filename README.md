# What is TermGet?
TermGet is a project made by PizzaLovingNerd, several others.

TermGet is a frontend for:

 - apt-get
 - xbps
 - dnf
 - eopkg
 - zypper
 - pacman
 - yaourt
 - homebrew
 - apm
 - pkg
 - chromebrew
 - emerge
 - pip
 - chocolatey [(Work In Progress)](https://gitlab.com/TermGet/TermGet-Windows)

## What are some of it's features

 - Simple, easy to use interface
 - Works in the command line
 - Supports a lot of package managers
 - Feature rich
 - Works on any Linux/ChromeOS/Mac distribution (and soon Windows)
 - Great for introducing package management to new users
 - Code is easy to modify.